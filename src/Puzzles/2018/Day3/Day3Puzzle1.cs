using System.Linq;

namespace Puzzles._2018.Day3
{
    public class Day3Puzzle3 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day);

            var parsed = Helper.ParseData(data);

            var overlaps = parsed.SelectMany(x => x.Fields).GroupBy(x => x).Where(x => x.Count() > 1);

            return overlaps.Count().ToString();
        }
        
        public int Year => 2018;
        public int Day => 3;
        public int Puzzle => 1;
    }
}