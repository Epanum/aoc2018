using System.Linq;

namespace Puzzles._2018.Day3
{
    public class Day3Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day);
            
            var parsed = Helper.ParseData(data);

            var singleFields = parsed
                .SelectMany(x => x.Fields)
                .GroupBy(x => x)
                .Where(x => x.Count() == 1)
                .Select(x => x.Key)
                .ToHashSet();

            var result = parsed.FirstOrDefault(x => x.Fields.All(y => singleFields.Contains(y)));

            return result?.Id;
        }

        public int Year => 2018;
        public int Day => 3;
        public int Puzzle => 2;
    }
}