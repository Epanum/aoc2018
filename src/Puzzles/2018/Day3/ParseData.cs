using System;
using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day3
{
    public static class Helper
    {
        public static Claim[] ParseData(string data)
        {
            return data.Split(Environment.NewLine).Select(x =>
            {
                var first = x.Split("@");

                var id = first[0].Trim();

                var second = first[1].Split(":");

                var coordinates = second[0].Split(",");

                var size = second[1].Split("x");
                
                return new Claim(
                    id, 
                    size: (int.Parse(size[0]), int.Parse(size[1])), 
                    coordinate: (int.Parse(coordinates[0]), int.Parse(coordinates[1])));
            }).ToArray();
        } 
    }
    
    public class Claim
    {
        public Claim(
            string id, 
            (int left, int top) coordinate, 
            (int x, int y) size)
        {
            Id = id;
            Coordinate = coordinate;
            Size = size;

            HashSet<(int, int)> fields = new HashSet<(int, int)>();
            for (int i = 1; i <= Size.x; i++)
            {
                for (int j = 1; j <= Size.y; j++)
                {
                    var field = (coordinate.left + i, coordinate.top + j);

                    fields.Add(field);
                }
            }

            Fields = fields;
        }
        
        public string Id { get; }
        public (int left, int top) Coordinate { get; }
        public (int x, int y) Size { get; }

        public HashSet<(int x, int y)> Fields { get; }
    }
}