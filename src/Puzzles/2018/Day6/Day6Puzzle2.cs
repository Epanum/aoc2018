using System;
using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day6
{
    public class Day6Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day, test).Split(Environment.NewLine).Select(x =>
            {
                var split = x.Split(",");
                return (int.Parse(split[0]), int.Parse(split[1]));
            }).OrderBy(x => x.Item1).ThenBy(x => x.Item2).ToHashSet();

            var largestX = data.Max(x => x.Item1);
            var largestY = data.Max(x => x.Item2);

            var result = new Dictionary<(int x, int y), List<(int x, int y, int distance)>>();
            
            foreach (var coordinate in data)
            {
                for (int i = 0; i < largestX; i++)
                {
                    for (int j = 0; j < largestY; j++)
                    {
                        var key = (i, j);
                        
                        var distance = ManhattanDistance(key, coordinate);

                        var value = (coordinate.Item1, coordinate.Item2, distance);
                        
                        if (result.ContainsKey(key))
                        {
                            result[key].Add(value);
                        }
                        else
                        {
                            result.Add(key, new List<(int x, int y, int distance)>() {value});
                        }
                    }
                }
            }

            var withinregion10000 = result.Where(x => x.Value.Sum(d => d.distance) < (test ? 32 : 10000)).Count();

            return withinregion10000.ToString();
        }

        public static int ManhattanDistance((int x, int y) first, (int x, int y) last)
        {
            return Math.Abs(first.x - last.x) + Math.Abs(first.y - last.y);
        }
        public int Year => 2018;
        public int Day => 6;
        public int Puzzle => 2;
    }
}