using System;
using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day7
{
    public class Day7Puzzle1 : ChallengeBase, IPuzzle 
    {
        private class Step
        {
            public char Blocking { get; set; }
            public char Item { get; set; }

            public (char[] valid, char[] invalid) Valid(HashSet<char> handledItems, HashSet<char> firstItems)
            {
                // Step contains first node in tree
                if (firstItems.Contains(Blocking) && !handledItems.Contains(Blocking))
                    return (new []{Blocking}, new char[]{});
                
                // no handled items yet, skip step
                if (!handledItems.Any())
                    return (new char[]{}, new char[]{});                    

                // step has already been handled
                if (handledItems.Contains(Item))
                    return (new char[]{}, new char[]{});
                
                // blocking step has been handled
                if (handledItems.Contains(Blocking))
                    return (new []{Item}, new char[]{});
                
                // return item as invalid to remove from total array
                return (new char[]{}, new []{Item});
            }
        }
        
        public string Run(bool test)
        {
            var data = LoadData(Year,Day, test);

            var parsed = data.Split(Environment.NewLine).Select(x =>
            {
                var parts = x.Split(" ");

                var before = parts[1][0];

                var step = parts[7][0];

                return (before, step);
            }).ToArray();

            var mapped = parsed.Select(x => new Step() {Item = x.Item2, Blocking = x.Item1}).ToArray();

            var count = parsed.Select(x => new[] {x.Item1, x.Item2}).SelectMany(x => x).Distinct().Count();
            
            var keys = mapped.Select(x => x.Item).ToHashSet();
            var first = mapped.Select(x => x.Blocking).Where(x => !keys.Contains(x)).Distinct().ToHashSet();
            
            var result = new HashSet<char>();
            
            for (int i = 0; i < count; i++)
            {
                var invalid = new List<char>();
                
                var valid = mapped.SelectMany(x =>
                {
                    var p = x.Valid(result, firstItems: first);

                    invalid.AddRange(p.invalid);

                    return p.valid;
                }).ToList();
                
                valid.RemoveAll(x => invalid.Contains(x));

                if (valid.Any()){
                    result.Add(valid.OrderBy(x => x).FirstOrDefault());
                }
            }
            
            return new string(result.Select(x => x).ToArray());
        }
        public int Year => 2018;
        public int Day => 7;
        public int Puzzle => 1;
    }
}