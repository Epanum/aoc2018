using System.Linq;

namespace Puzzles._2018.Day4
{
    public class Day4Puzzle1 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = Helper.ParseData(LoadData(Year,Day));

            var dataGrouped = data
                .Where(x => x.Text == "falls asleep")
                .GroupBy(x => x.GuardId)
                .ToArray();
            
            var guardMostAsleep = dataGrouped
                .Select(x => (x.Key, x.Sum(y => y.Lasted.Minutes)))
                .OrderByDescending(x => x.Item2)
                .FirstOrDefault();

            var minuteMostAsleep = dataGrouped.First(x => x.Key == guardMostAsleep.Item1).SelectMany(x => x.Minutes).GroupBy(x => x)
                .OrderByDescending(x => x.Count()).First().Key;
            
            return (int.Parse(guardMostAsleep.Item1) * minuteMostAsleep).ToString();
        }
        public int Year => 2018;

        public int Day => 4;
        public int Puzzle => 1;
    }
}