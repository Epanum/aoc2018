using System.Linq;

namespace Puzzles._2018.Day4
{
    public class Day4Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = Helper.ParseData(LoadData(Year,Day));

            var dataGrouped = data
                .Where(x => x.Text == "falls asleep")
                .GroupBy(x => x.GuardId)
                .ToArray();
            
            var guardMinuteMostAsleep = dataGrouped
                .Select(x =>
                {
                    var match = x.SelectMany(y => y.Minutes).GroupBy(z => z).OrderByDescending(y => y.Count())
                        .FirstOrDefault();
                    return (x.Key, match.Count(), match.Key);
                })
                .OrderByDescending(x => x.Item2)
                .FirstOrDefault();
            
            return (int.Parse(guardMinuteMostAsleep.Item1) * guardMinuteMostAsleep.Item3).ToString();
        }
        public int Year => 2018;
        public int Day => 4;
        public int Puzzle => 1;
    }
}