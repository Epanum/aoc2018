using System;
using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day4
{
    public static class Helper
    {
        public static SleepRecord[] ParseData(string data)
        {
            // [1518-09-13 23:56] Guard #1499 begins shift
            // [1518-08-20 00:37] falls asleep
            var parsed = data.Split(Environment.NewLine).Select(x =>
            {
                var first = x.Split("]");
                var time = DateTime.Parse(first[0].Remove(0, 1));
                
                if (first[1].Contains("#"))
                {
                    var second = first[1].Split("#")[1].Split(" ");

                    var guardId = second[0];

                    var text = $"{second[1]} {second[2]}";
                    
                    return new SleepRecord(time, guardId, text);
                }
                else
                {
                    var text = first[1].Trim(' ');
                    return new SleepRecord(time, null, text);
                }
            }).OrderBy(x => x.Time).ToArray();

            var currentGuard = string.Empty;
            
            for (int i = 0; i < parsed.Length; i++)
            {
                var current = parsed[i];
                
                if (i+1 < parsed.Length)
                {
                    current.Lasted = parsed[i + 1].Time - current.Time;
                    current.Minutes = Enumerable.Range(current.Time.Minute, current.Lasted.Minutes).ToHashSet();
                }
                
                if (current.GuardId != null)
                {
                    currentGuard = current.GuardId;
                }

                if (current.GuardId == null)
                {
                    current.GuardId = currentGuard;
                }
            }
            
            return parsed.ToArray();
        }
    }

    public class SleepRecord
    {
        public SleepRecord(DateTime time, string guardId, string text)
        {
            Time = time;
            GuardId = guardId;
            Text = text;
        }

        public DateTime Time { get; }
        public string GuardId { get; set; }
        public string Text { get; } 
        public TimeSpan Lasted { get; set; }

        public HashSet<int> Minutes { get; set; }
    }
}