using System.Linq;

namespace Puzzles._2018.Day5
{
    public class Day5Puzzle1 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day, test).ToList();
            
            //dabAcCaCBAcCcaDA

            for (int i = 0; i < data.Count(); i++)
            {
                if (i + 1 == data.Count())
                    break;
                
                var current = data[i];
                var next = data[i + 1];

                // Same char
                if (char.ToUpperInvariant(current) == char.ToUpperInvariant(next))
                {
                    if (current != next)
                    {
                       data.RemoveAt(i);
                       data.RemoveAt(i);
                       i = i-2;
                       if (i < -1)
                       {
                           i = -1;
                       } 
                    }
                }
            }

            return data.Count().ToString();
        }
        public int Year => 2018;
        public int Day => 5;
        public int Puzzle => 1;
    }
}