using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day5
{
    public class Day5Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day, test).ToList();

            var uniqueUnits = data.Select(char.ToUpperInvariant).Distinct();

            var result = new Dictionary<char, int>();
            
            foreach (var unit in uniqueUnits)
            {
                var clone = data.ToList();
                
                for (int i = 0; i < clone.Count(); i++)
                {
                    if (i + 1 == clone.Count())
                        break;
                
                    var current = clone[i];
                    var next = clone[i + 1];

                    var currentUpper = char.ToUpperInvariant(current);

                    if (currentUpper == unit)
                    {
                        clone.RemoveAt(i);
                        i = i-2;
                        if (i < -1)
                        {
                            i = -1;
                        } 
                        continue;
                    }
                    
                    // Same char
                    if (currentUpper == char.ToUpperInvariant(next))
                    {
                        if (current != next)
                        {
                            clone.RemoveAt(i);
                            clone.RemoveAt(i);
                            i = i-2;
                            if (i < -1)
                            {
                                i = -1;
                            } 
                        }
                    }
                }   
                
               result.Add(unit, clone.Count);
            }

            var shortestString = result.OrderBy(x => x.Value).FirstOrDefault();
            
            return shortestString.Value.ToString();
        }
        public int Year => 2018;
        public int Day => 5;
        public int Puzzle => 2;
    }
}