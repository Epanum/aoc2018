using System;

namespace Puzzles._2018.Day2
{
    public class Day2Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day);

            var lines = data.Split(Environment.NewLine);

            foreach (var line in lines)
            {
                foreach (var li in lines)
                {
                    if (line.Length != li.Length)
                        continue;

                    var differ = 0;
                    
                    for (int i = 0; i < line.Length; i++)
                    {
                        if (line[i] != li[i])
                            differ++;
                        
                        if (differ > 1)
                            break;
                    }

                    if (differ == 1)
                    {
                        return $"{line} - {li}";
                    }
                }
            }

            return "no match";
        }

        public int Year => 2018;
        public int Day => 2;
        public int Puzzle => 2;
    }
}