using System;
using System.Linq;

namespace Puzzles._2018.Day2
{
    public class Day2Puzzle1 : ChallengeBase, IPuzzle 
    {
        public string Run(bool test)
        {
            var data = LoadData(Year, Day);

            var lines = data.Split(Environment.NewLine);

            var contains2Letters = 0;
            var contains3Letters = 0;

            foreach (var line in lines)
            {
                var charGroups = line.Select(x => (int) x).GroupBy(x => x).ToArray();
                
                if (charGroups.Any(x => x.Count().Equals(3)))
                    contains3Letters++;
                
                if (charGroups.Any(x => x.Count().Equals(2)))
                    contains2Letters++;
            }

            return (contains2Letters * contains3Letters).ToString();
        }

        public int Day => 2;
        public int Puzzle => 1;
        public int Year => 2018;
    }
}