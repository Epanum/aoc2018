using System;
using System.Collections.Generic;
using System.Linq;

namespace Puzzles._2018.Day1
{
    public class Day1Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year, Day);
            
            var lines = data.Split(Environment.NewLine).Select(int.Parse).ToArray();

            HashSet<int> reachedFrequencies = new HashSet<int>();

            var result = 0;
            
            do
            {
                foreach (var line in lines)
                {                
                    result += line;

                    if (reachedFrequencies.Contains(result))
                        return result.ToString();

                    reachedFrequencies.Add(result);
                }     
            } while (true);
        }

        public int Day => 1;
        public int Puzzle => 2;
        
        public int Year => 2018;
    }
}