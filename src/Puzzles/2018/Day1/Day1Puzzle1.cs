using System;

namespace Puzzles._2018.Day1
{
    public class Day1Puzzle1 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year,Day);

            var lines = data.Split(Environment.NewLine);

            var result = 0;

            foreach (var line in lines)
            {
                var val = int.Parse(line);

                result += val;
            }

            return result.ToString();
        }

        public int Year => 2018;

        public int Day => 1;
        public int Puzzle => 1;
    }
}