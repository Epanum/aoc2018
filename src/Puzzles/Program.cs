﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using Puzzles._2018;

namespace Puzzles
{
    class Program
    {
        static void Main(string[] args)
        {
            var parsedArgs = args.Select(x =>
            {
                var split = x.Split("=");
                return (split[0], split.Length > 1 ? split[1] : "");
            }).ToArray();

            var test = parsedArgs.Any(x => x.Item1 == "--test");

            var runPuzzles = parsedArgs.FirstOrDefault(x => x.Item1 == "--puzzle");

            var year = parsedArgs.FirstOrDefault(x => x.Item1 == "--year");

            var puzzlesToRun = new List<(int,int)>();
            
            if (!string.IsNullOrWhiteSpace(runPuzzles.Item2))
            {
                foreach (var puzzle in runPuzzles.Item2.Split(','))
                {
                    var dayPuzzle = puzzle.Split('.');

                    puzzlesToRun.Add((int.Parse(dayPuzzle[0]), int.Parse(dayPuzzle[1])));
                }
            }
            
            var puzzles = typeof(Program).Assembly.GetTypes()
                .Where(x => 
                    !x.IsInterface && 
                    !x.IsAbstract && 
                    x.GetConstructor(new Type[0]) != null &&
                    typeof(IPuzzle).IsAssignableFrom(x))
                .Select(x => (IPuzzle) Activator.CreateInstance(x))
                .Where(x => x.Year.ToString() == year.Item2)
                .OrderBy(x => x.Day)
                .ThenBy(x => x.Puzzle);

            foreach (var puzzle in puzzles)
            {
                if (!puzzlesToRun.Any() || puzzlesToRun.Contains((puzzle.Day, puzzle.Puzzle)))
                {
                    var result = TimeExecution(() => puzzle.Run(test));
                
                    Console.WriteLine($"{puzzle.Day}.{puzzle.Puzzle} - Time: {result.executionTime.TotalMilliseconds}ms - Result: {result.result}");   
                }
            }
        }

        private static (string result, TimeSpan executionTime) TimeExecution(Func<string> method)
        {
            var stopWatch = new Stopwatch();
            
            stopWatch.Start();
            var result = method.Invoke();
            stopWatch.Stop();

            return (result, stopWatch.Elapsed);
        }
    }
}