using System.IO;

namespace Puzzles
{
    public abstract class ChallengeBase : IChallengeBase
    {
        public string LoadData(int year, int day, bool test = false)
        {
            var postfix = test ? "Test" : "";
            
            return File.ReadAllText($"./{year}/Day{day}/Data{postfix}.txt");
        }
    }
}