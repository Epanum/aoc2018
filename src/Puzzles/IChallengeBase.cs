namespace Puzzles
{
    public interface IChallengeBase
    {
        string LoadData(int year, int day, bool test = false);
    }
}