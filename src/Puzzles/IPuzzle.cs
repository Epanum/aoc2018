namespace Puzzles
{
    public interface IPuzzle
    {
        string Run(bool test);
        int Year { get; }
        int Day { get; }
        int Puzzle { get; }
    }
}