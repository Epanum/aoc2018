using System;
using System.Linq;

namespace Puzzles._2020.Day1
{
    public class Day1Puzzle1 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year, Day, test);

            var lines = data.Split(Environment.NewLine).Select(x => int.Parse(x)).ToArray();

            for (int i = 0; i < lines.Length; i++)
            {
                var val = lines[i];
                
                for (int j = 0; j < lines.Length; j++)
                {
                    if (j == i)
                        continue;
                    
                    var val2 = lines[j];

                    if (val + val2 == 2020)
                    {
                        return (val * val2).ToString();
                    }
                }
            }

            return string.Empty;
        }

        public int Year => 2020;

        public int Day => 1;
        public int Puzzle => 1;
    }
}