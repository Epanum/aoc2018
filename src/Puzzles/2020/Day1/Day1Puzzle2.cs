using System;
using System.Linq;

namespace Puzzles._2020.Day1
{
    public class Day1Puzzle2 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year, Day, test);

            var lines = data.Split(Environment.NewLine).Select(x => int.Parse(x)).ToArray();

            for (int i = 0; i < lines.Length; i++)
            {
                var val = lines[i];
                
                for (int j = 0; j < lines.Length; j++)
                {
                    if (j == i)
                        continue;
                    
                    var val2 = lines[j];

                    for (int k = 0; k < lines.Length; k++)
                    {
                        if (j == k || k == i)
                            continue;
                        
                        var val3 = lines[k];
                        
                        if (val + val2 + val3 == 2020)
                        {
                            return (val * val2 * val3).ToString();
                        }
                    }
                }
            }

            return string.Empty;
        }

        public int Year => 2020;

        public int Day => 1;
        public int Puzzle => 2;
    }
}