namespace Puzzles._2020.Day2
{
    public class Day2Puzzle1 : ChallengeBase, IPuzzle
    {
        public string Run(bool test)
        {
            var data = LoadData(Year, Day, test);
        }

        public int Year => 2020;
        public int Day => 2;
        public int Puzzle => 1;
    }
}